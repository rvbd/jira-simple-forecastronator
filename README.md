# The Simple JIRA Forecastron

## Introduction
I build this forecasting tool to help me (and others) forecast what might happen in a current or future sprint based om historical data

## Running Locally
JIRA ticket printer is designed to run under vagrant hence there are some dependencies. Please note that because original intention is to run this locally, the code is not security hardened. 
Although we should get some benefits from the codeigniter framework.

### Dependencies
Install the following before you start
1. Virtualbox (https://www.virtualbox.org/)
2. Vagrant (https://www.vagrantup.com/)

### Installations
1. Clone the code (git clone https://rvbd@bitbucket.org/rvbd/jira-simple-estimator.git)
2. Go to the box directory
3. Run: vagrant up
4. The software is set to run on port 8088
5. Go to: http://localhost:8088/setup.php
6. Enter http://localhost:8088/ as the base url
7. Enter your JIRA installation domain as the JIRA url (eg.  http://myjirainstallation.local/)
8. Once submitted, simply go to http://localhost:8088 
9. You are good to go! Happy printing

## Running as a website
It's of course very possible to run this software as a website. A possible scenario would be installing this ticket printer within in-premise server which have access to the JIRA server.

### Installation
1. Clone the code (git clone https://rvbd@bitbucket.org/rvbd/jira-simple-estimator.git)
2. Upload the contents of the website folder to your host
3. Visit the setup.php. Eg. http://yourticketdomain.com/setup.php
4. Enter your domain name (or IP) as the base url
5. Enter your JIRA installation domain as the JIRA url (eg.  http://yourticketdomain.com/)
6. Once submitted, delete setup.php just for added security
7. Go to your domain and you should be greeted by the search screen
8. Happy printing


# Acknowledements

This tool wouldn't have been possible without these work

1. CodeIgniter Framework (https://codeigniter.com/)
2. JIRA REST API Client by Chobie (https://github.com/chobie/jira-api-restclient)
3. Monte carlo simulation spreadsheet (http://scrumage.com/blog/2015/09/agile-project-forecasting-the-monte-carlo-method/)

# Copyright
Copyright 2017 Arvy

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
