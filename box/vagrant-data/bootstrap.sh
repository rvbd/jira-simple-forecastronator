#!/usr/bin/env bash

#install all the packages
sudo apt-get update

# ### INSTALL WEB SERVER

sudo apt-get -y install apache2
sudo apt-get -y install apache2-utils

# ### PHP SETUP
sudo apt-get -y install php php-mcrypt php-memcached php-curl php-gd php-zip
sudo phpenmod mcrypt

sudo apt-get install -y libapache2-mod-php

sudo a2enmod rewrite
sudo a2enmod headers


#make sure vagrant is in www-data group
sudo chown www-data:www-data /vagrant

sudo rm -rf /var/www/html

sudo ln -fs /vagrant /var/www/html


#lastly setup our Configuration files
echo "Setting up configuration files"

sudo cp /vagrant-data/000-default.conf /etc/apache2/sites-enabled/000-default.conf

# lastly reload all necessary services
sudo service apache2 restart
sudo service ssh reload

echo "Configuration files setup done default url is localhost:8088"
echo "Hit http://localhost:8088/setup.php on your browser for 1st time setup"
