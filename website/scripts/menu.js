$(function() {
	$(".forecast-start").click(function(e) {
		e.preventDefault();
		$(this).val("Please wait...talking to JIRA and crunching numbers...might take a while...");
		$(this).prop("disabled", true);
		$("#forecastron-menu").submit();
		$(".loader").show();
	});
	
	$("#link_generator").click(function(e) {
		e.preventDefault();
		var serializedForecast = $("#forecastron-menu").serialize();
		
		$.ajax({
			method: "GET",
			url: linkGenerateorUrl,
			data: serializedForecast,
			success: function(quickLink) {
				$("#quick-link").val(quickLink);
			}
		 });
	});
	
	$("#quick-link").click(function() {
		$(this).select();
	})
});