$(function() {
	var labels = [];
	var data = [];

	for (var monteCarloForecastKey in monteCarloForecast) {
		var sprint_no = parseInt(monteCarloForecast[monteCarloForecastKey].sprint_no) - 1;
		if (sprint_no == 0) {
			labels.push("Target Sprint");
		} else {
			labels.push("Target Sprint + " + sprint_no);
		}
		 
		data.push(monteCarloForecast[monteCarloForecastKey].percent_completion);
	}

	var ctx = document.getElementById("monteCarloChart").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'pie',
		data: {
			labels: labels,
			datasets: [{
				label: 'Sprints',
				data: data,
				backgroundColor: [
					 'rgba(255, 99, 132, 0.3)',
					 'rgba(54, 162, 235, 0.3)',
					 'rgba(255, 206, 86, 0.3)',
					 'rgba(75, 192, 192, 0.3)',
					 'rgba(153, 102, 255, 0.3)',
					 'rgba(255, 159, 64, 0.3)',
					 'rgba(255, 100, 64, 0.3)',
					 'rgba(196, 70, 64, 0.3)',
					 'rgba(255, 178, 249, 0.3)',
					 'rgba(0, 255, 198, 0.3)'
				],
			}]
		},
		options: {
			pieceLabel: {
				// render 'label', 'value', 'percentage', 'image' or custom function, default is 'percentage'
				render: function(args) {
					return args.label + " (" + args.value + '%)'
				},

				// precision for percentage, default is 0
				precision: 0,

				// identifies whether or not labels of value 0 are displayed, default is false
				showZero: true,

				// font size, default is defaultFontSize
				fontSize: 10,

				// font color, can be color array for each data or function for dynamic color, default is defaultFontColor
				fontColor: '#4F5155',

				// font style, default is defaultFontStyle
				fontStyle: 'normal',

				// font family, default is defaultFontFamily
				fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

				// draw label in arc, default is false
				arc: false,

				// position to draw label, available value is 'default', 'border' and 'outside'
				// default is 'default'
				position: 'outside',

				// draw label even it's overlap, default is false
				overlap: true
			}
		}
	});
});
