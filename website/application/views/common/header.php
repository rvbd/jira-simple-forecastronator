<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Simple JIRA Forecastron</title>
	<script src="//use.edgefonts.net/orbitron:n3,n5,n7,n9.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pure/1.0.0/pure-min.css" />
	<link rel="stylesheet" type="text/css" href="<?=$this->config->base_url()?>/style.css">
	
</head>
<body>

<div id="container">
	<h1>Simple JIRA Forecastron</h1>
		<?php
	if (isset($error_message) && $error_message) { ?>
			<p class="error"><?=$error_message?></p>
	<?php } ?>
	<div class="pure-menu pure-menu-horizontal">
		 <ul class="pure-menu-list">
			  <li class="pure-menu-item"><a href="<?=$this->config->base_url()?>" class="pure-menu-link">Main Menu</a></li>
		 </ul>
	</div>