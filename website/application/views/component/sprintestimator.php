<h2>
	Monte Carlo and Average Velocity forecast results may differ. We have both methods so that you can take into account the difference and make
	the right call with your team.
</h2>
<br />
<div class="pure-g">
	<div class="pure-u-1-3">
		<h2>Historical Data</h2>
		<p>Base data used for forecasting</p>
		<table class="pure-table historical-sprint-table">
			<thead>
				<tr>
					<th>Sprint Name</th>
					<th>No of Issues Completed</th>
					<th>Velocity</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($historical_sprints_data as $historical_sprint_name => $historical_sprint_data) { ?>
				<tr>
					<td><?=strtoupper($historical_sprint_name)?></td>
					<td><?=count($historical_sprint_data["tickets"])?></td>
					<td><?=$historical_sprint_data["velocity"]?></td>
				</tr>
				
				<?php } ?>
			</tbody>
		</table>
	</div>
	<div class="pure-u-1-3">
		<h2>Monte Carlo Simulation Forecast for <?=$target_sprint_names?></h2>
		<p>Simulates the sprints based on historical data of the number of tickets</p>
		<table class="pure-table monte-carlo-chart-table">
			<thead>
				<tr>
					<th>Sprint Number</th>
					<th>Chance of Completion</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$max_sprints = 0;
				foreach($monte_carlo_forecast as $monte_carlo_forecast_sprint) { 
				?>
				<tr>
					<td>
						<?php 
							$sprint_plus = ($monte_carlo_forecast_sprint["sprint_no"] - 1);
							if (!$sprint_plus) {
								echo "Completed in target sprint";
							} else {
								echo "Completed in target sprint + " . $sprint_plus;
							}

							$max_sprints = $monte_carlo_forecast_sprint["sprint_no"];
						?>
					</td>
					<td><?=$monte_carlo_forecast_sprint["confidence"]?>%</td>
				</tr>
				<?php } ?>
				<tr>
					<td>
						<strong>Number of Required Sprints to Completion</strong>
					</td>
					<td>
						<strong><?=$max_sprints?></strong>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<div class="monteCarloChart">
							<canvas id="monteCarloChart" width="320" height="320"></canvas>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="pure-u-1-3">
		<h2>Average Velocity Forecast for <?=$target_sprint_names?></h2>
		<p>Simulates the sprints required using historical velocity data and uses average ticket points for unestimated tickets</p>
		<table class="pure-table">
			<tbody>
				<tr>
					<th>Average Velocity</th>
					<td><?=round($average_velocity_forecast["average_velocity"], 2)?></td>
				</tr>
				<tr>
					<th>Projected Total Points</th>
					<td><?=round($average_velocity_forecast["total_points_projected"], 2)?></td>
				</tr>
				<tr>
					<th>Number of Required Sprints to Completion</th>
					<td><?=$average_velocity_forecast["total_projected_sprints"]?></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<script>
	var monteCarloForecast = <?=json_encode($monte_carlo_forecast)?>
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
<script src="<?=$this->config->base_url()?>scripts/piecelabel.js"></script>
<script src="<?=$this->config->base_url()?>scripts/montecarlo_chart.js"></script>