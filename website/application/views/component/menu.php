<br />
<em>JIRA URL: <?=$jira_end_point?></em>
<div class="search-box-container">
	<h2>Forecast by Sprint</h2>
	<form method="post" action="<?=$this->config->base_url()?>sprintestimator/estimatesprintwork/" class="pure-form pure-form-aligned" id="forecastron-menu">
		<fieldset>
			<div class="pure-control-group">
				<label for="project_code">Project Code: </label>
				<input type="text" name="project_code" id="project_code" value="<?=$form_input_data["project_code"]?>" />
			</div>
			
			<div class="pure-control-group">
				<label for="sprintnames">Sprint Names to Forecast (comma separated): </label>
				<input type="text" name="sprintnames" id="sprintnames" value="<?=$form_input_data["sprintnames"]?>" />
			</div>

			<div class="pure-control-group">
				<label for="historical_sprints">Historical sprints (comma separated): </label>
				<input type="text" class="pure-u-2-3" name="historical_sprints" id="historical_sprints" value="<?=$form_input_data["historical_sprints"]?>"/>
			</div>
			
			<div class="pure-control-group">
				<label for="team_capacity">Team capacity in percentage: </label>
				<input type="text" name="team_capacity" class="pure-u-1-24" id="team_capacity" value="<?=$form_input_data["team_capacity"]?>" /> %
			</div>
			
			<div class="pure-control-group">
				<label for="days_in_sprint">Days in each Sprint: </label>
				<input type="text" name="days_in_sprint" class="pure-u-1-24" id="days_in_sprint" value="<?=$form_input_data["days_in_sprint"]?>" /> days
			</div>
			
			<div class="pure-control-group">
				<input type="submit" value="Forecast!" class="pure-button pure-button-primary forecast-start" />
				<div class="loader">
					<img src="https://media.giphy.com/media/l3nWhI38IWDofyDrW/giphy.gif" />
				</div>
			</div>
		</fieldset>
	</form>
</div>
<div class="search-box-container">
	<h2>Create Quick Link</h2>
	<p>If you wish to share the parameters with your team, generate the link here.</p>
	<form class="pure-form">
		<input type="text" class="pure-u-2-3" id="quick-link"/>
		<button class="pure-button pure-button-primary" id="link_generator">Generate Link</button>
	</form>
</div>
<script>
	var progressUrl = "<?=$this->config->base_url() . "sprintestimator/get_progress"?>";
	var linkGenerateorUrl = "<?=$this->config->base_url() . "sprintestimator/get_quick_url"?>";
</script>
<script src="<?=$this->config->base_url()?>scripts/menu.js"></script>