<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chobie\Jira\Api;
use chobie\Jira\Api\Authentication\Basic;
use chobie\Jira\Issues\Walker;

class Sprintestimator extends CI_Controller {
	
	private $historical_sprints = array();
	private $project_code = "";
	private $team_capacity = 80;
	private $days_in_sprint = 10;
	private $tickets_in_sprint_to_forecast = null;
	
	/**
	 * Default landing page 
	 */
	public function index($base64inputparams = false)
	{
		$form_input_data = array(
			 "project_code" => "",
			 "sprintnames" => "",
			 "historical_sprints" => "",
			 "team_capacity" => 80, //default to 80% cap
			 "days_in_sprint" => 10 //default
		);
		
		if ($base64inputparams) {
			$form_input_data = array_merge($form_input_data, $this->decode_quick_url($base64inputparams));
		}
		
		$this->load->helper('url');

		$jira_url = $this->session->jira_url;

		if (!$jira_url) {
			$jira_url = $this->config->item('jira_base_url');
		}

		$view_data = array(
			"error_message" => $this->session->error_message,
			"jira_end_point" => $jira_url,
			"form_input_data" => $form_input_data
		);

		$this->load->view('common/header', $view_data);
		

		if (!$this->session->username || !$this->session->password) {
			$this->load->view('component/loginbox', $view_data);

		} else {

			$this->load->view('component/menu', $view_data);
		}

		
		$this->load->view('common/footer');
	}

	public function login()
	{
		$this->load->helper('url');

		$jira_url = $this->config->item('jira_base_url');

		if ($this->input->post('jira_end_point') && !$jira_url) {
			$jira_url = $this->input->post('jira_end_point');
		}

		if ($this->input->post('username') && $this->input->post('password') && $jira_url) {

			try {
				// try to login
				$api = new \chobie\Jira\Api(
					$jira_url,
					new \chobie\Jira\Api\Authentication\Basic($this->input->post('username'), $this->input->post('password'))
				);

				$api->getResolutions();
			} catch (Exception $e) {
				$this->session->set_flashdata("error_message", $e->getMessage());				
			}

			$this->session->set_userdata(array(
				"username" => $this->input->post('username'),
				"password" => $this->input->post('password'),
				"jira_url" => $jira_url
			));
		}

		redirect($this->config->base_url());
	}

	public function estimateSprintWork() 
	{
		$this->load->helper('url');

		if ($this->input->post("sprintnames") && 
				  $this->input->post("historical_sprints") && 
				  $this->input->post("project_code") && 
				  $this->input->post("team_capacity") &&
				  $this->input->post("days_in_sprint")
				) {
			
			
			
			// now find out the historical sprints so that we can construct the data
			$historical_sprints = explode(",", $this->input->post("historical_sprints"));
			
			$this->historical_sprints = $historical_sprints;
			$this->project_code = $this->input->post("project_code");
			$this->team_capacity = $this->input->post("team_capacity");
			$this->days_in_sprint = $this->input->post("days_in_sprint");
			
			// firstly get the data of the sprint to forecast
			// construct the sprint names
			$target_sprints = explode(",", $this->input->post("sprintnames"));
			$this->tickets_in_sprint_to_forecast = $this->get_tickets_in_sprint($target_sprints, $this->project_code);
			

			$historical_sprints_data = $this->get_historical_sprints_data($historical_sprints, $this->input->post("project_code"));
			
			// we have the raw data, let's do monte carlo simulations
			$monte_carlo_simulation_forecast = $this->get_monte_carlo_simulation_forecast($historical_sprints_data);
			
			$this->load->view('common/header');	
			$this->load->view('component/sprintestimator', array(
				"historical_sprints_data" => $historical_sprints_data,
				"monte_carlo_forecast" => $monte_carlo_simulation_forecast,
				"average_velocity_forecast" => $this->get_req_sprints_by_average_velocity_calculations($historical_sprints_data),
				"target_sprint_names" => trim(implode($target_sprints, ", "), ",")
			));
			$this->load->view('common/footer');
			
		} else {
			$this->session->set_flashdata("error_message", "All fields are required");
			redirect($this->config->base_url());
		}
	}
	
	private function get_req_sprints_by_average_velocity_calculations($historical_sprints_data) {
		// search what is the ticket average velocity
		// this is to pad out the tickets that doesn't have story points in our 
		// target sprint
		$total_issues = 0;
		$total_story_points = 0;
		$total_velocity = 0;
		foreach ($historical_sprints_data as $historical_sprint) {
			foreach ($historical_sprint["tickets"] as $issue) {
				$total_issues++;
				$total_story_points += $issue->get("Story Points");
			}
			
			$total_velocity += $historical_sprint["velocity"];
		}
		
		$average_story_points = $total_story_points / $total_issues;
		$average_velocity = $total_velocity / count($historical_sprints_data);
                
                // need to use the team capacity settings to reduce or pump up the team capacity
                $average_velocity = $this->team_capacity / 100 * $average_velocity;
		
		$total_points_in_target_sprint = 0;
		foreach ($this->tickets_in_sprint_to_forecast as $issue_in_sprint) {
			if ($issue_in_sprint["points"] == 0) {
				$total_points_in_target_sprint += $average_story_points;
			} else {
				$total_points_in_target_sprint += $issue_in_sprint["points"];
			}
		}
		
		// now calculate number of required sprints based on total points in the sprint
		// and average velocity based on history
		
		$total_projected_sprints = round($total_points_in_target_sprint / $average_velocity, 2);
		
		return array(
			"average_velocity" => $average_velocity,
			"total_projected_sprints" => $total_projected_sprints,
			"total_points_projected" => $total_points_in_target_sprint
		);
	}
	
	private function get_monte_carlo_simulation_forecast($historical_sprints_data) {
		$this->set_progress("Simulating sprints...");

		// we have the raw sprints data, calculate the program focus based on the days in sprint and the number of completed tickets
		$program_focus = $this->get_program_focus($historical_sprints_data);

		// get the cycle times for each of the sprints
		$sprints_cycle_time = $this->get_sprints_cycle_time($historical_sprints_data, $program_focus);
		$sprints_cycle_time_values = array_values($sprints_cycle_time);

		// now we simulate the sprints
		// for now we use 1000 samples
		$sprints_simulation_data = array();
		$simulated_days_per_items = array();
		for ($x = 0; $x < 2000; $x++) {
			$total_sim_days = 0;
			for ($simulated_cycle_time = 0; $simulated_cycle_time < count($sprints_cycle_time_values); $simulated_cycle_time++) {
				$total_sim_days += $sprints_cycle_time_values[array_rand($sprints_cycle_time_values)];
			}
			
			// simulated takt time
			$days_per_items = $total_sim_days / count($sprints_cycle_time_values);
			
			$sprints_simulation_data[] = array(
				 "total_sim_days" => $total_sim_days,
				 "days_per_items" => $days_per_items
			);
			
			$simulated_days_per_items[] = $days_per_items;
		}
		
		// we've now grabbed the historical data, now we need to get the current sprint data
		$number_of_tickets_current_sprint = count($this->tickets_in_sprint_to_forecast);
		
		// simulate the current sprint 1000 times using the simulated days per items
		$simulated_forecasts = array();
		for ($x = 0; $x < 2000; $x++) {
			$rand_simulated_days_per_items = $simulated_days_per_items[array_rand($simulated_days_per_items)];
			$simulated_sprint_number = (int) round(($rand_simulated_days_per_items * $number_of_tickets_current_sprint) /  $this->days_in_sprint);
			
			// because we are rounding we might end up with zero, so anything zero should be 1 as we can't have complete
			// an item in 0 sprints
			if (!$simulated_sprint_number) {
				$simulated_sprint_number = 1;
			}

			if (!array_key_exists($simulated_sprint_number, $simulated_forecasts)) {
				$simulated_forecasts[$simulated_sprint_number] = 0;
			}
			
			$simulated_forecasts[$simulated_sprint_number] += 1; // number of tickets to complete in sprint x
			
		}

		ksort($simulated_forecasts);
		
		// calculate confidence
		$probability_data = array();
		$prev_percent_completion = 0;
		foreach ($simulated_forecasts as $sprint_no => $num_of_tickets_completed) {
			$percent_completion = ($num_of_tickets_completed / 2000 * 100);
			$probability_data[] = array(
				 "sprint_no" => $sprint_no,
				 "percent_completion" => $percent_completion,
				 "confidence" => $percent_completion + $prev_percent_completion
			);
			
			$prev_percent_completion = $percent_completion;
		}

		return $probability_data;
		
	}
	
	private function get_tickets_in_sprint(Array $sprint_names, $project_code) {

		$this->set_progress("Getting target sprint data...");
		
		// construct sprint names as a string
		$sprint_names_ql = "";
		foreach ($sprint_names as $sprint_name) {
			$sprint_names_ql .= '"' . trim($sprint_name) . '",';
		}
		
		$sprint_names_ql = trim($sprint_names_ql,",");
		
		
		$jql = 'status not in ("done", "won\'t do") and sprint in (' . $sprint_names_ql . ') and type not in (Sub-task, epic) and project = "' . $project_code . '" order by rank asc';
		
		try {
			// try to login
			$api = new \chobie\Jira\Api(
				$this->session->jira_url,
				new \chobie\Jira\Api\Authentication\Basic($this->session->username, $this->session->password)
			);

			$walker = new \chobie\Jira\Issues\Walker($api);
			$walker->push(
				$jql
			);

			$issues_collection = array();
			
			if (!count($walker)) {
				$this->session->set_flashdata("error_message", "No sprint data found");
				redirect($this->config->base_url());
			}

			foreach ( $walker as $issue ) {

				$issue_type = $issue->getIssueType();

				$issues_collection[] = array(
					"description" => $issue->getDescription(),
					"acceptance_criteria" => $issue->get('Acceptance Criteria'),
					"summary" => $issue->getSummary(),
					"key" => $issue->getKey(),
					"points" => $issue->get('Story Points'),
					"type" => $issue_type['name'],
					"type_style" => "type-" . str_replace(' ', '-', strtolower($issue_type['name']))
				);
			}

			return $issues_collection;
			
		} catch (Exception $e) {
			$this->session->set_flashdata("error_message", $e->getMessage());
			$this->session->unset_userdata('username');
			$this->session->unset_userdata('password');
			$this->session->unset_userdata('jira_url');
			redirect($this->config->base_url());
		}
	}
	
	private function get_sprints_cycle_time($historical_sprints_data, $program_focus) {
		$sprints_cycle_time = array();
		
		foreach ($historical_sprints_data as $historical_sprint_name => $historical_sprint_data) {
			if ($program_focus[$historical_sprint_name]) {
				$sprints_cycle_time[$historical_sprint_name] = $this->days_in_sprint / $program_focus[$historical_sprint_name];
			} else {
				$sprints_cycle_time[$historical_sprint_name] = 0;
			}
		}
		
		return $sprints_cycle_time;
	}
	
	/**
	 * Program focus basically simulates the work that would have been completed
	 * in the sprint based on the team capacity data
	 * 
	 * @param type $historical_sprints_data
	 * @return type
	 */
	private function get_program_focus($historical_sprints_data) {
		$program_focus = array();
		foreach ($historical_sprints_data as $historical_sprint_name => $historical_sprint_data) {
			$program_focus[$historical_sprint_name] = count($historical_sprint_data["tickets"]) * $this->team_capacity / 100;
		}		
		return $program_focus;
	}
	
	private function get_historical_sprints_data(Array $historical_sprints_name, $project_code) {
		$issues_collection = array();
		
		foreach ($historical_sprints_name as $historical_sprint_name) {

			$this->set_progress("Getting sprint " . $historical_sprint_name . " data");

			$historical_sprint_name = trim($historical_sprint_name);
			$jql = 'status was done and sprint = "' . $historical_sprint_name . '" and type not in (Sub-task, epic) and project = "' . $project_code . '" order by rank asc';
		
			$issues_collection[$historical_sprint_name] = array(
				 "tickets" => array(),
				 "velocity" => 0
			);
			
			try {
				// try to login
				
				$api = new \chobie\Jira\Api(
					$this->session->jira_url,
					new \chobie\Jira\Api\Authentication\Basic($this->session->username, $this->session->password)
				);
								
				// search directly without walker
				$search_results = $api->api(
							$api::REQUEST_GET, 
							"/rest/api/2/search",
							array(
								'jql' => $jql,
								'startAt' => 0,
								'maxResults' => 9999,
								'fields' => "*navigable",
								'expand' => "changelog"
							)
						);
				
				foreach ($search_results->getIssues() as $issue) {
					$include_in_issue_collection = true;
					$issue_expanded_info = $issue->getExpandedInformation();
					if ($issue_expanded_info["changelog"] && $issue_expanded_info["changelog"]["histories"]) {
						foreach ($issue_expanded_info["changelog"]["histories"] as $issue_history) {
							if ($issue_history["items"]) {
								foreach ($issue_history["items"] as $history_item) {
									// we have to check as well if this has been moved to the sprint back, in this case the fromString
									// would have been a list of sprint names with comma separation
									$historic_fromString_array = explode(",", $history_item["fromString"]);
									$last_fromString = trim(end($historic_fromString_array));
									
									if ($history_item["field"] == "Sprint" && strtolower($last_fromString) == trim(strtolower($historical_sprint_name))) {
										$include_in_issue_collection = false; // item transitioned out of the sprint, so don't include
										
									}
									
									// we have to check as well if this has been moved to the sprint back, in this case the toString
									// would have been a list of sprint names with comma separation
									$historic_toString_array = explode(",", $history_item["toString"]);
									$last_toString = trim(end($historic_toString_array));
									
									if ($history_item["field"] == "Sprint" && strtolower($last_toString) == trim(strtolower($historical_sprint_name))) {
										$include_in_issue_collection = true; // item transitioned into the sprint, so don't include
									}
								}
							}
						}
					}
					
					if ($include_in_issue_collection) {
						$issues_collection[$historical_sprint_name]["tickets"][] = $issue;
						$issues_collection[$historical_sprint_name]["velocity"] += $issue->get("Story Points");
					}
				}

			} catch (Exception $e) {
				$this->session->set_flashdata("error_message", $e->getMessage());
				$this->session->unset_userdata('username');
				$this->session->unset_userdata('password');
				$this->session->unset_userdata('jira_url');
				redirect($this->config->base_url());
			}
		}
			
		return $issues_collection;
	}


	public function end() 
	{
		$this->load->helper('url');

		$this->session->unset_userdata('username');
		$this->session->unset_userdata('password');
		$this->session->unset_userdata('jira_url');

		redirect($this->config->base_url());
	}

	private function set_progress($message) 
	{
		$this->session->set_flashdata("progress", $message);
	}

	public function get_progress() {
		echo $this->session->progress;
	}
	
	public function get_quick_url() {
		// convert the params from get into base64
		echo $this->config->base_url() . "sprintestimator/index/" . urlencode(base64_encode(serialize($this->input->get())));
	}
	
	private function decode_quick_url($encodedData) {
		return unserialize(base64_decode(urldecode($encodedData)));
	}
}
