<?php
define('BASEPATH', '/');
if (file_exists("application/config/config.php")) {
	include_once("application/config/config.php");
} else {
	include_once("application/config/config.sample.php");
}

// write config if we want to setup
if ($_POST["action"] == "generate_config") {
	$config["base_url"] = rtrim($_POST["config_base_url"], '/') . '/';
	$config["jira_base_url"] = rtrim($_POST["config_jira_base_url"], '/') . '/';

	// keep the jira base URL empty if there's really nothing
	if (!$_POST["config_jira_base_url"]) {
		$config["jira_base_url"] = '';
	}

	// write the config file
	$config_header = "<?php defined('BASEPATH') OR exit('No direct script access allowed'); ";
	$config_body = '$config = ' . var_export($config, true) . ';';

	file_put_contents("application/config/config.php", $config_header . $config_body);
}
?>

<html>
	<head>
		<style>
			body {
				font-family: 'arial';	
			}

			em {
				color: red;
				padding-bottom: 20px;
				display: block;
			}

			.setup-container {
				width: 500px;
				margin: 0px auto;
				border: 1px solid #eee;
				-webkit-box-shadow: -1px 15px 73px -10px rgba(0,0,0,0.75);
				-moz-box-shadow: -1px 15px 73px -10px rgba(0,0,0,0.75);
				box-shadow: -1px 15px 73px -10px rgba(0,0,0,0.75);
				padding: 15px;
			}
		</style>
	</head>
	<body>
		<div class="setup-container">
			<h1>JIRA Ticket Printer Quick Setup </h1>
			<em>Delete this file after setup if you deploy on production!</em>
			<form method="POST">
				<table>
					<tr>
						<td>
							Base URL (set this up as your server domain)
						</td>
						<td>
							<input type="text" value="<?=$config["base_url"]?>" name="config_base_url" />
						</td>
					</tr>
					<tr>
						<td>
							JIRA URL
						</td>
						<td>
							<input type="text" value="<?=$config["jira_base_url"]?>" name="config_jira_base_url" />
						</td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" value="Save" /></td>
					</tr>
				</table>
				<input type="hidden" name="action" value="generate_config" />
			</form>
		</div>
	</body>
</html>

